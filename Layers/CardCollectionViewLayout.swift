//
//  CardCollectionViewLayout.swift
//  Layers
//
//  Created by Rene Cacheaux on 1/19/15.
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import UIKit

// NOTE: This custom layout is built specifically for the AsyncDisplayKit tutorial. If you would like
//  to use this layout outside this project you may end up needing to make modifications.
//  However, this code is a good starting point.

@objc
protocol UICollectionViewDelegateCardLayout : UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
}

protocol CardCollectionViewLayoutMetrics {
  func numberOfRowsForNumberOfItems(numberOfItems: Int) -> Int
  func rowForItemAtIndex(index: Int) -> Int
  func columnForItemAtIndex(index: Int) -> Int
  func indexForItemAboveItemAtIndex(index: Int) -> Int
  func numberOfColumns() -> Int
}

class TwoColumnCardLayoutMetrics: CardCollectionViewLayoutMetrics {
  func numberOfRowsForNumberOfItems(numberOfItems: Int) -> Int {
    var isOdd: Bool = numberOfItems%2 > 0
    var numberOfRows = numberOfItems/2
    if isOdd {
      numberOfRows++
    }
    return numberOfRows
  }
  
  func rowForItemAtIndex(index: Int) -> Int {
    return ((index + 1)/2 + (index + 1)%2) - 1
  }
  
  func columnForItemAtIndex(index: Int) -> Int {
    return index%2
  }
  
  func indexForItemAboveItemAtIndex(index: Int) -> Int {
    var aboveItemIndex = index - 2
    return aboveItemIndex >= 0 ? aboveItemIndex : index
  }
  
  func numberOfColumns() -> Int {
    return 2
  }
}

class OneColumnCardLayoutMetrics: CardCollectionViewLayoutMetrics {
  func numberOfRowsForNumberOfItems(numberOfItems: Int) -> Int {
    return numberOfItems
  }
  
  func rowForItemAtIndex(index: Int) -> Int {
    return index
  }
  
  func columnForItemAtIndex(index: Int) -> Int {
    return 0
  }
  
  func indexForItemAboveItemAtIndex(index: Int) -> Int {
    var aboveItemIndex = index - 1
    return aboveItemIndex >= 0 ? aboveItemIndex : index
  }
  
  func numberOfColumns() -> Int {
    return 1
  }
}

enum CardLayoutType {
  case OneColumn
  case TwoColumn
  
  func metrics() -> CardCollectionViewLayoutMetrics {
    switch self {
    case OneColumn:
      return OneColumnCardLayoutMetrics()
    case TwoColumn:
      return TwoColumnCardLayoutMetrics()
    }
  }
}

class CardCollectionViewLayout: UICollectionViewLayout {
  var allLayoutAttributes = [UICollectionViewLayoutAttributes]()
  let cellDefaultHeight = 300
  let cellWidth = Int(FrameCalculator.cardWidth)
  let interCellVerticalSpacing = 10
  let interCellHorizontalSpacing = 10
  var contentMaxY: CGFloat = 0
  var layoutType: CardLayoutType
  var layoutMetrics: CardCollectionViewLayoutMetrics
  
  // TODO: This is a hack, ASCollection view should have a solid way to call this.
  func scrollDirection() -> UICollectionViewScrollDirection {
    return UICollectionViewScrollDirection.Vertical
  }
  
  init(type: CardLayoutType) {
    layoutType = type
    layoutMetrics = type.metrics()
    super.init()
  }
  
  override init() {
    layoutType = .TwoColumn
    layoutMetrics = layoutType.metrics()
    super.init()
  }
  
  required init(coder aDecoder: NSCoder) {
    layoutType = .TwoColumn
    layoutMetrics = layoutType.metrics()
    super.init(coder: aDecoder)
  }
  
  override func prepareLayout() {
    super.prepareLayout()
    
    // TODO: Optimize. This gets called once for each batch add.
    if let collectionView = self.collectionView {
      if collectionView.frame.size.width < CGFloat((self.cellWidth * 2) + interCellHorizontalSpacing) {
        layoutType = .OneColumn
        layoutMetrics = layoutType.metrics()
      }
    }
    populateLayoutAttributes()
    
  }
  
  func populateLayoutAttributes() {
    if self.collectionView == nil {
      return
    }
    let collectionView = self.collectionView!
    
    // Calculate left margin max x.
    let totalWidthOfCellsInARow = layoutMetrics.numberOfColumns() * cellWidth
    let totalSpaceBetweenCellsInARow = interCellHorizontalSpacing * max(0, layoutMetrics.numberOfColumns() - 1)
    let totalCellAndSpaceWidth = totalWidthOfCellsInARow + totalSpaceBetweenCellsInARow
    let totalHorizontalMargin = collectionView.frame.size.width - CGFloat(totalCellAndSpaceWidth)
    let leftMarginMaxX = totalHorizontalMargin / CGFloat(2.0)
    
    allLayoutAttributes.removeAll(keepCapacity: true)
    if collectionView.numberOfSections() > 0 {
      var column0BottomY: CGFloat = 0
      var column1BottomY: CGFloat = 0
      for i in 0 ..< collectionView.numberOfItemsInSection(0) {
        let indexPath = NSIndexPath(forItem: i, inSection: 0)
        let la = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
        let row = layoutMetrics.rowForItemAtIndex(i)
        let column = layoutMetrics.columnForItemAtIndex(i)
        
        let asyncCollectionView = collectionView as ASCollectionView
        let cellSize = asyncCollectionView.calculatedSizeForNodeAtIndexPath(indexPath)
        
        let x = ((Int(cellSize.width) + interCellHorizontalSpacing) * column) + Int(leftMarginMaxX)
        
        var previousBottomY: CGFloat = 0
        if column == 0 {
          previousBottomY = column0BottomY
        } else if column == 1 {
          previousBottomY = column1BottomY
        }
        
        let y = Int(previousBottomY) + interCellVerticalSpacing
        
        la.frame = CGRectZero
        la.frame.origin = CGPoint(x: x, y: y)
        la.frame.size = cellSize
        
        if column == 0 {
          column0BottomY = ceil(la.frame.maxY)
        } else if column == 1 {
          column1BottomY = ceil(la.frame.maxY)
        }
        
        allLayoutAttributes.append(la)
        if la.frame.maxY > contentMaxY {
          contentMaxY = ceil(la.frame.maxY)
        }
      }
    }
  }
  
  override func collectionViewContentSize() -> CGSize {
    if self.collectionView == nil {
      return CGSizeZero
    }
    let collectionView = self.collectionView!
    return CGSize(width: collectionView.frame.size.width, height: contentMaxY)
  }
  
  override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
    println("layoutAttributesForElementsInRect")
    var layoutAttributes = [UICollectionViewLayoutAttributes]()
    for i in 0 ..< allLayoutAttributes.count {
      let la = allLayoutAttributes[i]
      if rect.contains(la.frame) {
        layoutAttributes.append(la)
      }
    }
    return allLayoutAttributes
  }
  
  override func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes! {
    return allLayoutAttributes[indexPath.item]
  }
}
