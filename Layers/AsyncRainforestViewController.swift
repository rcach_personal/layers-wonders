//
//  AsyncRainforestViewController.swift
//  Layers
//
//  Created by Rene Cacheaux on 1/22/15.
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import UIKit

class AsyncRainforestViewController: UIViewController, ASCollectionViewDataSource, ASCollectionViewDelegate {
  let rainforestCardsInfo = getAllCardInfo()
  let asyncCollectionView = ASCollectionView(frame: CGRectZero, collectionViewLayout: CardCollectionViewLayout())
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(asyncCollectionView)
    asyncCollectionView.asyncDataSource = self
    asyncCollectionView.asyncDelegate = self
  }
  
  override func viewDidLayoutSubviews() {
    asyncCollectionView.frame = view.bounds
  }
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView!) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let count = rainforestCardsInfo.count
    return count
  }
  
  func collectionView(collectionView: ASCollectionView!, nodeForItemAtIndexPath indexPath: NSIndexPath!) -> ASCellNode! {
    let cardInfo = rainforestCardsInfo[indexPath.item]
    let node = CardCellNode(cardInfo: cardInfo)
    return node
  }
}
