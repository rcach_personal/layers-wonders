//
//  CardCellNode.swift
//  Layers
//
//  Created by Rene Cacheaux on 1/19/15.
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

class CardCellNode: ASCellNode {
  let containerNode: ASDisplayNode
  let backgroundImageNode: ASImageNode
  let featureImageNode: ASImageNode
  let titleTextNode: ASTextNode
  let descriptionTextNode: ASTextNode
  let gradientNode: GradientNode
  
  let cardImageSize: CGSize
  
  init(cardInfo: RainforestCardInfo) {
    let cardImage = UIImage(named: cardInfo.imageName)!
    
    cardImageSize = cardImage.size
    
    containerNode = ASDisplayNode()
    containerNode.layerBacked = true
    containerNode.shouldRasterizeDescendants = true
    
    backgroundImageNode = ASImageNode()
    backgroundImageNode.image = cardImage
    backgroundImageNode.contentMode = .ScaleAspectFill
    backgroundImageNode.layerBacked = true
    var imageNode = backgroundImageNode
    backgroundImageNode.imageModificationBlock = { [weak imageNode] input in
      if input == nil {
        return input
      }
      let didCancelBlur: () -> Bool = {
        var isCancelled = true
        if let strongBackgroundImageNode = imageNode {
          let isCancelledClosure = {
            isCancelled = strongBackgroundImageNode.displaySuspended
          }
          if NSThread.isMainThread() {
            isCancelledClosure()
          } else {
            dispatch_sync(dispatch_get_main_queue(), isCancelledClosure)
          }
        }
        return isCancelled
      }
      if let blurredImage = input.applyBlurWithRadius(30, tintColor: UIColor(white: 0.5, alpha: 0.3),
        saturationDeltaFactor: 1.8, maskImage: nil,
        didCancel:didCancelBlur) {
          return blurredImage
      } else {
        return input
      }
    }
    
    featureImageNode = ASImageNode()
    featureImageNode.layerBacked = true
    featureImageNode.contentMode = .ScaleAspectFit
    featureImageNode.image = cardImage
    
    titleTextNode = ASTextNode()
    titleTextNode.layerBacked = true
    titleTextNode.backgroundColor = UIColor.clearColor()
    titleTextNode.attributedString = NSAttributedString.attributedStringForTitleText(cardInfo.name)
    
    descriptionTextNode = ASTextNode()
    descriptionTextNode.layerBacked = true
    descriptionTextNode.backgroundColor = UIColor.clearColor()
    descriptionTextNode.attributedString =
      NSAttributedString.attributedStringForDescriptionText(cardInfo.description)
    
    gradientNode = GradientNode()
    gradientNode.opaque = false
    gradientNode.layerBacked = true
    
    super.init()

    containerNode.addSubnode(backgroundImageNode)
    containerNode.addSubnode(featureImageNode)
    containerNode.addSubnode(gradientNode)
    containerNode.addSubnode(titleTextNode)
    containerNode.addSubnode(descriptionTextNode)
    addSubnode(containerNode)
  }
  
  override func layout() {
    super.layout()

    let simulatedBounds = CGRect(x: 0, y: 0, width: calculatedSize.width, height: calculatedSize.height)
    
    containerNode.frame = simulatedBounds
    backgroundImageNode.frame = FrameCalculator.frameForBackgroundImage(
      containerBounds: simulatedBounds)
    featureImageNode.frame = FrameCalculator.frameForFeatureImage(featureImageSize: cardImageSize,
      containerFrameWidth: calculatedSize.width)
    titleTextNode.frame = FrameCalculator.frameForTitleText(containerBounds: simulatedBounds,
      featureImageFrame: featureImageNode.frame)
    descriptionTextNode.frame = FrameCalculator.frameForDescriptionText(containerBounds:
      simulatedBounds, featureImageFrame: featureImageNode.frame)
    gradientNode.frame = FrameCalculator.frameForGradient(featureImageFrame: featureImageNode.frame)
  }
  
  override func calculateSizeThatFits(constrainedSize: CGSize) -> CGSize {
    return FrameCalculator.sizeThatFits(CGSize(width: 320, height: NSIntegerMax), withImageSize: cardImageSize)
  }
}
