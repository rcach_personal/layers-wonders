//
//  ASCollectionViewLayoutController.mm
//  Pods
//
//  Created by Rene Cacheaux on 1/23/15.
//
//

#import "ASCollectionViewLayoutController.h"

#include <map>
#include <vector>
#include <cassert>

#import "ASAssert.h"

@interface ASCollectionViewLayoutController () {
  std::vector<std::vector<CGSize> > _nodeSizes;
  
  std::pair<int, int> _visibleRangeStartPos;
  std::pair<int, int> _visibleRangeEndPos;
  
  std::pair<int, int> _workingRangeStartPos;
  std::pair<int, int> _workingRangeEndPos;
  
  ASFlowLayoutDirection _layoutDirection;
  
  NSInteger _currentPage;
}
@property(nonatomic, strong) UICollectionViewLayout *layout;
@end

// TODO: Follow ASDK conventions with @properties
// TODO: Determine object (ARC) ownership for layout, should it be strong?
@implementation ASCollectionViewLayoutController


- (instancetype)initWithScrollOption:(ASFlowLayoutDirection)layoutDirection layout:(UICollectionViewLayout *)layout {
  if (!(self = [super initWithScrollOption:layoutDirection])) {
    return nil;
  }
  _layout = layout;
  return self;
}

/**
 * IndexPath array for the element in the working range.
 */
- (NSSet *)workingRangeIndexPathsForScrolling:(enum ASScrollDirection)scrollDirection
                                 viewportSize:(CGSize)viewportSize {

  NSMutableSet *indexPathSet = [[NSMutableSet alloc] init];
  
  NSArray *layoutAttributes = [self.layout layoutAttributesForElementsInRect:[self workingRangeRectWithScrollDirection:scrollDirection viewportSize:viewportSize]];
  for (UICollectionViewLayoutAttributes *la in layoutAttributes) {
    [indexPathSet addObject:la.indexPath];
  }
  
  return indexPathSet;
}

- (CGRect)workingRangeRectWithScrollDirection:(enum ASScrollDirection)scrollDirection viewportSize:(CGSize)viewportSize {
  if (_layoutDirection == ASFlowLayoutDirectionHorizontal) {
    ASDisplayNodeAssert(scrollDirection == ASScrollDirectionNone || scrollDirection == ASScrollDirectionLeft || scrollDirection == ASScrollDirectionRight, @"Invalid scroll direction");
    
    CGFloat backScreens = scrollDirection == ASScrollDirectionLeft ? self.tuningParameters.leadingBufferScreenfuls : self.tuningParameters.trailingBufferScreenfuls;
    CGFloat frontScreens = scrollDirection == ASScrollDirectionLeft ? self.tuningParameters.trailingBufferScreenfuls : self.tuningParameters.leadingBufferScreenfuls;
    
    // TODO: is this all happening on the main thread? is it guarenteed?
    CGRect bounds = self.layout.collectionView.bounds;
    bounds = CGRectOffset(bounds, -(backScreens * viewportSize.width), 0);
    bounds = CGRectMake(bounds.origin.x, bounds.origin.y, ((backScreens + frontScreens) * viewportSize.width), bounds.size.height);
    return bounds;
  } else {
    ASDisplayNodeAssert(scrollDirection == ASScrollDirectionNone || scrollDirection == ASScrollDirectionUp || scrollDirection == ASScrollDirectionDown, @"Invalid scroll direction");
    
    int backScreens = scrollDirection == ASScrollDirectionUp ? self.tuningParameters.leadingBufferScreenfuls : self.tuningParameters.trailingBufferScreenfuls;
    int frontScreens = scrollDirection == ASScrollDirectionUp ? self.tuningParameters.trailingBufferScreenfuls : self.tuningParameters.leadingBufferScreenfuls;
    
    CGRect bounds = self.layout.collectionView.bounds;
    bounds = CGRectOffset(bounds, 0, -(backScreens * viewportSize.height));
    bounds = CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, ((backScreens + frontScreens) * viewportSize.height));
    return bounds;
  }
}

- (BOOL)shouldUpdateWorkingRangesForVisibleIndexPath:(NSArray *)indexPaths
                                        viewportSize:(CGSize)viewportSize {
  if (!indexPaths.count) {
    return NO;
  }
  
  std::pair<int, int> startPos, endPos;
  ASFindIndexPathRange(indexPaths, startPos, endPos);
  
  if (_workingRangeStartPos >= startPos || _workingRangeEndPos <= endPos) {
    return YES;
  }
  
  
  CGRect bounds = self.layout.collectionView.bounds;
  CGFloat maxY = CGRectGetMaxY(bounds);
  _currentPage = fmodf(maxY, bounds.size.height);
  
  NSLog(@"Current Page: %li", (long)_currentPage);
  
  return YES;
  //  return ASFlowLayoutDistance(startPos, _visibleRangeStartPos, _nodeSizes) > ASFlowLayoutDistance(_visibleRangeStartPos, _workingRangeStartPos, _nodeSizes) * kASFlowLayoutControllerRefreshingThreshold ||
  //         ASFlowLayoutDistance(endPos, _visibleRangeEndPos, _nodeSizes) > ASFlowLayoutDistance(_visibleRangeEndPos, _workingRangeEndPos, _nodeSizes) * kASFlowLayoutControllerRefreshingThreshold;
}

static void ASFindIndexPathRange(NSArray *indexPaths, std::pair<int, int> &startPos, std::pair<int, int> &endPos) {
  NSIndexPath *initialIndexPath = [indexPaths firstObject];
  startPos = endPos = {initialIndexPath.section, initialIndexPath.row};
  [indexPaths enumerateObjectsUsingBlock:^(NSIndexPath *indexPath, NSUInteger idx, BOOL *stop) {
    std::pair<int, int> p(indexPath.section, indexPath.row);
    startPos = MIN(startPos, p);
    endPos = MAX(endPos, p);
  }];
}




@end
