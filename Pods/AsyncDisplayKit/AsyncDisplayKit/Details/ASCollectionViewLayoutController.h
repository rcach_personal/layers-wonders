//
//  ASCollectionViewLayoutController.h
//  Pods
//
//  Created by Rene Cacheaux on 1/23/15.
//
//

#import <AsyncDisplayKit/ASFlowLayoutController.h>

@interface ASCollectionViewLayoutController : ASFlowLayoutController

@property (nonatomic, readonly, assign) ASFlowLayoutDirection layoutDirection;

- (instancetype)initWithScrollOption:(ASFlowLayoutDirection)layoutDirection layout:(UICollectionViewLayout *)layout;

@end
