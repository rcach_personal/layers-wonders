//
//  ViewController.swift
//  Wonders-Demo
//
//  Created by Rene Cacheaux on 1/24/15.
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import UIKit

class ViewControllerOne: UIViewController {
  let wonderImageNode = ASImageNode()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    wonderImageNode.image = UIImage(named: "image")
    view.addSubview(wonderImageNode.view)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    wonderImageNode.measure(view.frame.size)
    wonderImageNode.frame = CGRect(origin: CGPointZero, size: wonderImageNode.calculatedSize)
  }
}

class ViewControllerTwo: UIViewController {
  let wonderImageNode = ASImageNode()
  let mainQueue = dispatch_get_main_queue()
  let backgroundQueue = dispatch_queue_create("com.rwdevcon.wonders.background", DISPATCH_QUEUE_CONCURRENT)
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !wonderImageNode.nodeLoaded {
      loadImageNode()
    }
  }
  
  func loadImageNode() {
    let rootViewSize = view.frame.size
    dispatch_async(backgroundQueue) {
      self.wonderImageNode.image = UIImage(named: "image")
      self.wonderImageNode.measure(rootViewSize)
      self.wonderImageNode.frame = CGRect(origin: CGPointZero, size: self.wonderImageNode.calculatedSize)
      dispatch_async(self.mainQueue) {
        self.view.addSubview(self.wonderImageNode.view)
      }
    }
  }
}

class ViewControllerThree: UIViewController {
  let wonderImageNode = ASImageNode()
  let mainQueue = dispatch_get_main_queue()
  let backgroundQueue = dispatch_queue_create("com.rwdevcon.wonders.background", DISPATCH_QUEUE_CONCURRENT)
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !wonderImageNode.nodeLoaded {
      loadImageNode()
    }
  }
  
  func loadImageNode() {
    let rootViewSize = view.frame.size
    dispatch_async(backgroundQueue) {
      self.wonderImageNode.image = UIImage(named: "image")
      self.wonderImageNode.addTarget(self, action: "share", forControlEvents: ASControlNodeEvent.TouchUpInside)
      self.wonderImageNode.measure(rootViewSize)
      self.wonderImageNode.frame = CGRect(origin: CGPointZero, size: self.wonderImageNode.calculatedSize)
      dispatch_async(self.mainQueue) {
        self.view.addSubview(self.wonderImageNode.view)
      }
    }
  }
  
  func share() {
    let activityViewController = UIActivityViewController(activityItems: ["Check this out!"], applicationActivities: nil)
    activityViewController.modalPresentationStyle = .Popover
    presentViewController(activityViewController, animated: true, completion: nil)
  }
}

class ViewControllerFour: UIViewController {
  let wonderImageNode = ASImageNode()
  let titleTextNode = ASTextNode()
  let mainQueue = dispatch_get_main_queue()
  let backgroundQueue = dispatch_queue_create("com.rwdevcon.wonders.background", DISPATCH_QUEUE_CONCURRENT)
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !wonderImageNode.nodeLoaded {
      loadImageNode()
    }
  }
  
  func loadImageNode() {
    let rootViewSize = view.frame.size
    dispatch_async(backgroundQueue) {
      self.wonderImageNode.image = UIImage(named: "image")
      self.wonderImageNode.addTarget(self, action: "share", forControlEvents: ASControlNodeEvent.TouchUpInside)
      self.wonderImageNode.measure(rootViewSize)
      self.wonderImageNode.frame = CGRect(origin: CGPointZero, size: self.wonderImageNode.calculatedSize)
      
      self.titleTextNode.attributedString = NSAttributedString(string: "Jaguar")
      self.titleTextNode.measure(self.wonderImageNode.calculatedSize)
      self.titleTextNode.frame = CGRect(origin: CGPointZero, size: self.titleTextNode.calculatedSize)
      
      self.wonderImageNode.addSubnode(self.titleTextNode)
      
      dispatch_async(self.mainQueue) {
        self.view.addSubview(self.wonderImageNode.view)
      }
    }
  }
  
  func share() {
    let activityViewController = UIActivityViewController(activityItems: ["Check this out!"], applicationActivities: nil)
    activityViewController.modalPresentationStyle = .Popover
    presentViewController(activityViewController, animated: true, completion: nil)
  }
}

class ViewController: UIViewController {
  let wonderImageNode = ASImageNode()
  let titleTextNode = ASTextNode()
  let mainQueue = dispatch_get_main_queue()
  let backgroundQueue = dispatch_queue_create("com.rwdevcon.wonders.background", DISPATCH_QUEUE_CONCURRENT)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.blackColor()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !wonderImageNode.nodeLoaded {
      loadImageNode()
    }
  }
  
  func loadImageNode() {
    let rootViewSize = view.frame.size
    dispatch_async(backgroundQueue) {
      self.wonderImageNode.image = UIImage(named: "image")
      self.wonderImageNode.addTarget(self, action: "share", forControlEvents: ASControlNodeEvent.TouchUpInside)
      self.wonderImageNode.measure(rootViewSize)
      
      self.wonderImageNode.frame = CGRect(origin: CGPointZero, size: self.wonderImageNode.calculatedSize)
      
      dispatch_async(self.mainQueue) {
        self.view.addSubview(self.wonderImageNode.view)
      }
    }
    
    dispatch_async(backgroundQueue) {
      
      self.titleTextNode.attributedString = NSAttributedString.attributedStringForTitleText("Jaguar")
      self.titleTextNode.measure(rootViewSize)
      sleep(2)
      self.titleTextNode.frame = CGRect(origin: CGPoint(x: 20, y: 200), size: self.titleTextNode.calculatedSize)
      
      dispatch_async(self.mainQueue) {
        self.wonderImageNode.addSubnode(self.titleTextNode)
      }
    }
    
  }
  
  func share() {
    let activityViewController = UIActivityViewController(activityItems: ["Check this out!"], applicationActivities: nil)
    activityViewController.modalPresentationStyle = .Popover
    presentViewController(activityViewController, animated: true, completion: nil)
  }
}

// Present Popover on Touch
// -- Hit test slop

// Make it layer backed
